//alert("HELEEO");

/*
Functions
	-Functions in JavaScript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	-Functions are mostly created to create complicated tasks to run several lines of codes in succession

	-Functions are also used to prevent repeating lines/blocks of codes that perform the same task/function
*/

// Function Declaration
	//(Function Statement) defines a function with the specified parameters

/*
	Synatx:
		function functionName() {
			codeblock(statement)
		}

	function keyword - used to define a JavaScript functions
	functionName - the function name are used later in the code
	function block ({}) - the statements which comprise the body of the function. This is where the code to be executed
*/

function printName() {
	console.log("My name is John!");
}


//Function Invocation
/*
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
*/
//Invoke/Call the function that we declared
printName();

//declaredFunction(); Uncaught ReferenceError: declaredFunction is not defined


//Function Declaration vs Function Expressions

	//Function Declaration
		//A function can be created through function declaration by using the function keyword and adding the function name
		//Declared function are NOT executed immediately. They are "saved for later use" and will be executed later when they are invoked(called)

	declaredFunction();

	function declaredFunction() {
		console.log("Hello World from the declaredFunction()");
	}

	declaredFunction();

	//Function Expression
		//A function can also be stored in a variable. This is what we call a function expression

		//A function expression is an anonymous function assigned to the variableFunction

		//Anonymous function - a function without a name

	let variableFunction = function() {
		console.log("Hello from the variableFunction");
	}

	variableFunction();


	//Function Expression are ALWAYS invoked(called) using the variable name

	let funcExpression = function funcName () {
		console.log("Hello from the other side!");
	}

	funcExpression();

	//Can we reassign declared functions and function expressions to a NEW anonymous functions? Yes
	declaredFunction = function () {
		console.log("Updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function () {
		console.log("Updated funcExpression");
	}

	funcExpression();


	//We cannot re-assign a function expression initialized with a const keyword
	const constantFunc = function() {
		console.log("Initialize with CONST");
	}

	constantFunc();

/*	constantFunc = function() {
		console.log("Can we reassign?");
	}

	constantFunc();*/


	//Function Scoping
	/*
		Scope is the accessibility (visibility) of variables

		JS Variables has 3 types of scope
			1. Local/block scope
			2. Global scope
			3. Function scope
	*/

	{
		let localVar = "I am a local variable.";
	}

let globalVar = "I am a global variable";

console.log(globalVar);
//console.log(localVar);

	function showNames() {
		//Function scoped variables
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	//The variables, functionVar, functionConst, and functionLet are function scoped variables. They can only be accessed inside of the function they were declared in.
		// console.log(functionVar);
		// console.log(functionConst);
		// console.log(functionLet);


//Nested Function
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable "name" as they are within the same code block/scope
	function myNewFunction() {
		let name = "Maria";

		function nestedFunction(){
			let nestedFunction = "Jose";
			console.log(name);
		}
		// console.log(nestedName);
		nestedFunction();
	}
	myNewFunction();


//Function and Global Scoped Variables
	//Global Variable
	let globalName = "Erven Joshua";

	function myNewFunction2() {
		let nameInside = "Jenno";

		console.log(globalName);
	}

	myNewFunction2();

	// console.log(nameInside);


//Using alert();
	// alert("Hello World!");

	// function showSampleAlert() {
	// 	alert("Hello User!");
	// }

	// showSampleAlert();

	// console.log("Hello pows!");


//Using prompt();

	// let samplePrompt = prompt("Enter your name: ");
	// console.log("Hello, " + samplePrompt);


	function printWelcomeMessage() {
		let firstName = prompt("Enter your first name: ");
		let lastName = prompt("Enter your last name: ");

		console.log("Hello, " + firstName + ' ' + lastName + '!');
		console.log("Welcome to my page!");
	}

	// printWelcomeMessage();


//Function Naming Conventions
	//Name your functions in small caps followed by camelCase when naming variables and functions

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000");
	}

	displayCarInfo();

	//Function names should be definitive of the task it will perform. It usually contains a verb

	function getCourse(){
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourse();

	//Avoid generic names to avoid confusion within your code